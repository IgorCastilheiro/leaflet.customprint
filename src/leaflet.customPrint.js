

L.Control.CustomPrint = L.Control.extend({
    options: {
        position: 'topleft',
        title: 'Imprimir mapa',
        tooltip: 'Imprimir'
    },

    onAdd: function () {
        var container = L.DomUtil.create('div', 'leaflet-control-customPrint leaflet-bar leaflet-control');

        this.link = L.DomUtil.create('a', 'leaflet-control-customPrint-button leaflet-bar-part', container);
        this.link.href = '#';
        this.link.title = 'Imprimir mapa';
        L.DomEvent
            .on(this.link, 'click', L.DomEvent.stopPropagation)
            .on(this.link, 'click', L.DomEvent.preventDefault)
            .on(this.link, 'click', function() {
                var $template = $('<div></div>').load('bower_components/leaflet-custom-print/assets/printTemplate.html', function(){

                    var $map = $("#map").clone(true);

                    $map.find(".leaflet-control").addClass("_cpHidden");

                    $map.find(".leaflet-popup-pane").addClass("_cpHidden");

                    $template.find('.mapContainer').replaceWith($map);

                    //wait iframe to render
                    setTimeout(function(){
                        $template.printArea();
                    }, 400);

                });

            });
        return container;
    }
});

L.control.customPrint = function() {
    return new L.Control.CustomPrint();
};

